package lang.tree;

import java.util.ArrayList;
import java.util.List;

public class TreeNode {
    private String label;
    private boolean terminal;
    private List<TreeNode> children;

    public TreeNode(String label, boolean terminal) {
        this.label = label;
        this.terminal = terminal;
        if (!terminal) {
            children = new ArrayList<>();
        }
    }

    public void addChild(TreeNode child) {
        if (!terminal) {
            this.children.add(child);
        }
    }

    private String toString(int level) {
        StringBuilder result = new StringBuilder("");
        for (int i = 0; i < level; i++)
            result.append(' ');
        if (terminal) {
            result.append('[');
        }
        result.append(label);
        if (terminal) {
            result.append(']');
        }
        result.append('\n');
        if (!terminal) {
            for (TreeNode child : children) {
                result.append(child.toString(level + 1));
            }
        }
        return result.toString();
    }

    @Override
    public String toString() {
        return toString(0);
    }
}
