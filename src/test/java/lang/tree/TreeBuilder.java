package lang.tree;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.ParserRuleContext;

public class TreeBuilder {
    private static String getNodeName(ParserRuleContext node) {
        String className = node.getClass().getName();
        int dollarSign = className.indexOf('$');
        if (dollarSign >= 0) {
            className = className.substring(dollarSign + 1);
        }
        return className.replace("Context", "");
    }

    public static TreeNode buildTree(ParserRuleContext node) {
        TreeNode result = new TreeNode(getNodeName(node), false);
        if (node.children != null) {
            for (ParseTree t : node.children) {
                TreeNode child;
                if (t instanceof ParserRuleContext) {
                    child = buildTree((ParserRuleContext) t);
                } else {
                    child = new TreeNode(t.getText(), true);
                }
                result.addChild(child);
            }
        }
        return result;
    }
}
