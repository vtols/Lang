import lang.tree.TreeBuilder;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import parser.LangLexer;
import parser.LangParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParserTest {
    private String name;
    private String in;
    private String out;

    @Parameters(name = "{0}")
    public static List<Object[]> inputsAndOutputs() {
        List<Object[]> result = new ArrayList<>();
        try {
            InputStream contents = ParserTest.class.getClassLoader().getResourceAsStream("inputs");
            List<String> resources = IOUtils.readLines(contents, "UTF-8");
            contents.close();
            for (String resourceName : resources) {
                InputStream inStream =
                        ParserTest.class.getClassLoader().getResourceAsStream("inputs/" + resourceName);
                InputStream outStream =
                        ParserTest.class.getClassLoader().getResourceAsStream("outputs/" + resourceName);
                String inText = IOUtils.toString(inStream, "UTF-8");
                String outText = IOUtils.toString(outStream, "UTF-8");
                inStream.close();
                outStream.close();
                result.add(new Object[]{resourceName, inText, outText});
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public ParserTest(String name, String in, String out) {
        this.name = name;
        this.in = in;
        this.out = out;
    }

    @Test
    public void testParsing() {
        LangLexer lexer = new LangLexer(CharStreams.fromString(in));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        LangParser parser = new LangParser(tokens);
        LangParser.ProgramContext tree = parser.program();

        String actualOut = TreeBuilder.buildTree(tree).toString();

        assertEquals(out, actualOut);
    }
}
