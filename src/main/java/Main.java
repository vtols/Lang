import ast.data.Function;
import ast.exec.ExecutionContext;
import ast.exec.ProgramBuilder;
import ast.generator.Generator;
import ast.statement.FlowAction;
import ast.statement.Program;
import ast.statement.Statement;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import parser.LangLexer;
import parser.LangParser;

import java.io.IOException;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        LangLexer lexer;
        try {
            lexer = new LangLexer(CharStreams.fromFileName(args[0]));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        LangParser parser = new LangParser(tokens);
        LangParser.ProgramContext tree = parser.program();

        ProgramBuilder contextBuilder = new ProgramBuilder(tree);
        Program program = contextBuilder.build();
        ExecutionContext ctx = new ExecutionContext();

        ctx.addFunction(new Function(
                "println",
                Collections.singletonList("value"),
                new Statement(){
                    @Override
                    public void generate(Generator g) {

                    }

                    @Override
                    public FlowAction execute(ExecutionContext ctx) {
                        System.out.println(ctx.getVariable("value"));
                        return FlowAction.NONE;
                    }
                }
        ));

        program.execute(ctx);

        Generator g = new Generator();
        program.generate(g);
        System.out.println(g.getCode());
    }
}
