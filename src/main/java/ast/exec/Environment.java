package ast.exec;

public interface Environment {
    Object getVariable(String name);
    Environment getParent();
}
