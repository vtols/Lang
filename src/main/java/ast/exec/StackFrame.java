package ast.exec;

import java.util.HashMap;
import java.util.Map;

public class StackFrame implements Environment {
    private Map<String, Object> variables = new HashMap<>();
    private Object returnValue = 0;
    private Environment bind = null;

    public void setBind(Environment bind) {
        this.bind = bind;
    }

    public void setReturnValue(Object returnValue) {
        this.returnValue = returnValue;
    }

    public Object getReturnValue() {
        return returnValue;
    }

    public void setVariable(String name, Object value) {
        variables.put(name, value);
    }

    public Object getVariable(String name) {
        Object result = variables.getOrDefault(name, null);
        if (result == null && bind != null) {
            result = bind.getVariable(name);
        }
        return result;
    }

    @Override
    public Environment getParent() {
        return bind;
    }
}
