package ast.exec;

import ast.data.Function;
import ast.expression.*;
import ast.statement.*;
import org.antlr.v4.runtime.tree.TerminalNode;
import parser.LangBaseVisitor;
import parser.LangParser;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProgramBuilder {
    private ExecutionContext execCtx = new ExecutionContext();
    private LangParser.ProgramContext programContext;

    public ProgramBuilder(LangParser.ProgramContext programContext) {
        this.programContext = programContext;
    }
    
    public Program build() {
        return buildProgram();
    }
    
    private Program buildProgram() {
        List<Function> functions = new ArrayList<>();
        for (LangParser.FunctionContext functionContext : programContext.function()) {
            Function f = buildFunction(functionContext);
            functions.add(f);
        }
        return new Program(functions);
    }

    private String getText(TerminalNode node) {
        return node.getSymbol().getText();
    }

    private List<String> getText(List<TerminalNode> nodes) {
        return nodes.stream()
                .map(node -> node.getSymbol().getText())
                .collect(Collectors.toList());
    }

    private Function buildFunction(LangParser.FunctionContext functionContext) {
        String functionName = getText(functionContext.ID());
        List<String> functionArguments = getText(functionContext.idList().ID());
        Block body = buildBlock(functionContext.block());
        return new Function(functionName, functionArguments, body);
    }

    private Block buildBlock(LangParser.BlockContext block) {
        List<LangParser.StatementContext> contexts = block.statement();
        List<Statement> statementList =
                contexts.stream().map(this::buildStatement).collect(Collectors.toList());
        return new Block(statementList);
    }

    private Statement buildStatement(LangParser.StatementContext statement) {
        return new StatementVisitor().visit(statement);
    }

    private Expression buildExpression(LangParser.ExprContext expr) {
        return new ExprVisitor().visit(expr);
    }

    private List<Expression> buildExpressions(List<LangParser.ExprContext> expressions) {
        return expressions.stream()
                .map(this::buildExpression)
                .collect(Collectors.toList());
    }

    private If buildIf(LangParser.IfTailContext ifTail) {
        Expression conditionExpression = buildExpression(ifTail.condition);
        Statement trueBody = buildBlock(ifTail.block());
        Statement falseBody = null;
        LangParser.ElseIfContext elseIF = ifTail.elseIf();
        if (elseIF != null) {
            if (elseIF instanceof LangParser.ElseContext) {
                falseBody = buildBlock(((LangParser.ElseContext) elseIF).block());
            } else if (elseIF instanceof  LangParser.ElifContext) {
                falseBody = buildIf(((LangParser.ElifContext) elseIF).ifTail());
            }
        }
        return new If(conditionExpression, trueBody, falseBody);
    }

    private class StatementVisitor extends LangBaseVisitor<Statement> {
        @Override
        public Statement visitAssignment(LangParser.AssignmentContext ctx) {
            return new Assign(getText(ctx.ID()), buildExpression(ctx.expr()));
        }

        @Override
        public Statement visitExprStatement(LangParser.ExprStatementContext ctx) {
            return new ExpressionStatement(buildExpression(ctx.expr()));
        }

        @Override
        public Statement visitIfStatement(LangParser.IfStatementContext ctx) {
            return buildIf(ctx.ifTail());
        }

        @Override
        public Statement visitWhileStatement(LangParser.WhileStatementContext ctx) {
            return new While(buildExpression(ctx.condition), buildBlock(ctx.block()));
        }

        @Override
        public Statement visitForStatement(LangParser.ForStatementContext ctx) {
            return new For(
                    ctx.var.getText(),
                    buildExpression(ctx.list),
                    buildBlock(ctx.block()));
        }

        @Override
        public Statement visitBreakStatement(LangParser.BreakStatementContext ctx) {
            return new Break();
        }

        @Override
        public Statement visitContinueStatement(LangParser.ContinueStatementContext ctx) {
            return new Continue();
        }

        @Override
        public Statement visitReturnStatement(LangParser.ReturnStatementContext ctx) {
            LangParser.ExprContext exprContext = ctx.expr();
            Expression returnExpression = null;
            if (exprContext != null)
                returnExpression = buildExpression(exprContext);
            return new Return(returnExpression);
        }
    }

    private class ExprVisitor extends LangBaseVisitor<Expression> {
        @Override
        public Expression visitLambda(LangParser.LambdaContext ctx) {
            return new Lambda(getText(ctx.idList().ID()), buildBlock(ctx.block()));
        }

        @Override
        public Expression visitAddExpr(LangParser.AddExprContext ctx) {
            LangParser.Add_opContext addOp = ctx.add_op();
            BinaryOperation op = null;
            if (addOp.PLUS() != null) {
                op = BinaryOperation.ADD;
            } else if (addOp.MINUS() != null) {
                op = BinaryOperation.SUBTRACT;
            }
            return new Binary(super.visit(ctx.left), super.visit(ctx.right), op);
        }

        @Override
        public Expression visitMulExpr(LangParser.MulExprContext ctx) {
            LangParser.Mul_opContext mulOp = ctx.mul_op();
            BinaryOperation op = null;
            if (mulOp.MULT() != null) {
                op = BinaryOperation.MULTIPLY;
            } else if (mulOp.DIV() != null) {
                op = BinaryOperation.DIVISION;
            } else if (mulOp.REM() != null) {
                op = BinaryOperation.REMAINDER;
            }
            return new Binary(super.visit(ctx.left), super.visit(ctx.right), op);
        }

        @Override
        public Expression visitUnary(LangParser.UnaryContext ctx) {
            LangParser.Unary_opContext unaryOp = ctx.unary_op();
            UnaryOperation op = null;
            if (unaryOp.NOT() != null) {
                op = UnaryOperation.NOT;
            } else if (unaryOp.MINUS() != null) {
                op = UnaryOperation.MINUS;
            }
            return new Unary(super.visit(ctx.expr()), op);
        }

        @Override
        public Expression visitCompareExpr(LangParser.CompareExprContext ctx) {
            LangParser.Compare_opContext compareOp = ctx.compare_op();
            BinaryOperation op = null;
            if (compareOp.EQ() != null) {
                op = BinaryOperation.EQ;
            } else if (compareOp.NE() != null) {
                op = BinaryOperation.NE;
            } else if (compareOp.LE() != null) {
                op = BinaryOperation.LE;
            } else if (compareOp.LEQ() != null) {
                op = BinaryOperation.LEQ;
            } else if (compareOp.GE() != null) {
                op = BinaryOperation.GE;
            } else if (compareOp.GEQ() != null) {
                op = BinaryOperation.GEQ;
            }
            return new Binary(super.visit(ctx.left), super.visit(ctx.right), op);
        }

        @Override
        public Expression visitNumber(LangParser.NumberContext ctx) {
            return new Constant(Integer.valueOf(getText(ctx.Num())));
        }

        @Override
        public Expression visitString(LangParser.StringContext ctx) {
            String strLiteral = ctx.STR().getSymbol().getText();
            return new Constant(strLiteral.substring(1, strLiteral.length() - 1));
        }

        @Override
        public Expression visitVariable(LangParser.VariableContext ctx) {
            return new Variable(getText(ctx.ID()));
        }

        @Override
        public Expression visitFunctionCall(LangParser.FunctionCallContext ctx) {
            return new FunctionCall(buildExpression(ctx.func), buildExpressions(ctx.exprList().expr()));
        }

        @Override
        public Expression visitList(LangParser.ListContext ctx) {
            return new ListExpression(buildExpressions(ctx.exprList().expr()));
        }
    }
}
