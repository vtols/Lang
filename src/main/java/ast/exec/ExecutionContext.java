package ast.exec;

import ast.data.Function;
import ast.expression.FunctionCall;
import ast.expression.Variable;

import java.util.*;

public class ExecutionContext {
    private Map<String, Function> functions = new HashMap<>();
    private Stack<StackFrame> frames = new Stack<>();
    private StackFrame topFrame = null;

    public Object start() {
        FunctionCall mainCall = new FunctionCall(new Variable("main"), Collections.emptyList());
        return mainCall.evaluate(this);
    }

    public void addFunction(Function f) {
        functions.put(f.getName(), f);
    }

    public void setBinding(Environment bind) {
        topFrame.setBind(bind);
    }

    public Environment createBinding() {
        return topFrame;
    }

    public void setVariable(String name, Object value) {
        topFrame.setVariable(name, value);
    }

    public Object getVariable(String name) {
        Object value = null;
        if (topFrame != null) {
            value = topFrame.getVariable(name);
        }
        if (value == null) {
            value = functions.getOrDefault(name, null);
        }
        return value;
    }

    public Function getFunction(String name) {
        Object func = getVariable(name);
        return (Function) func;
    }

    public void pushFrame() {
        StackFrame newFrame = new StackFrame();
        frames.push(newFrame);
        topFrame = newFrame;
    }

    public Object popFrame() {
        StackFrame oldFrame = frames.pop();
        if (frames.empty()) {
            topFrame = null;
        } else {
            topFrame = frames.peek();
        }
        return oldFrame.getReturnValue();
    }

    public void setReturnValue(Object value) {
        topFrame.setReturnValue(value);
    }

    public boolean isTrue(Object obj) {
        return Boolean.TRUE.equals(obj) ||
                Integer.valueOf(1).equals(obj) ||
                ((obj instanceof List) && !((List) obj).isEmpty()) ||
                ((obj instanceof String) && !((String) obj).isEmpty());
    }

    @SuppressWarnings("unchecked")
    public Iterator<Object> getIterator(Object iterable) {
        if (iterable instanceof Integer) {
            return new IntIterator((Integer) iterable);
        } else if (iterable instanceof Iterable) {
            return ((Iterable<Object>) iterable).iterator();
        } else if (iterable instanceof String) {
            return new StrIterator((String) iterable);
        }
        return null;
    }

    private class StrIterator implements Iterator<Object> {
        private int current = 0;
        private String s;

        StrIterator(String s) {
            this.s = s;
        }

        @Override
        public boolean hasNext() {
            return current < s.length();
        }

        @Override
        public Object next() {
            return String.valueOf(s.charAt(current++));
        }
    }

    private class IntIterator implements Iterator<Object> {
        private int current = 0;
        private int end;

        IntIterator(int end) {
            this.end = end;
        }

        @Override
        public boolean hasNext() {
            return current <= end;
        }

        @Override
        public Integer next() {
            return current++;
        }
    }
}
