package ast.expression;

public enum BinaryOperation {
    ADD, SUBTRACT, MULTIPLY, DIVISION, REMAINDER,
    EQ, NE, LEQ, LE, GEQ, GE
}
