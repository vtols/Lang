package ast.expression;

public enum UnaryOperation {
    NOT, MINUS
}
