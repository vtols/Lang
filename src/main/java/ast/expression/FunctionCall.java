package ast.expression;

import ast.data.Function;
import ast.exec.ExecutionContext;
import ast.generator.Generator;
import ast.statement.FlowAction;
import ast.statement.Statement;

import java.util.ArrayList;
import java.util.List;

public class FunctionCall implements Expression {
    private Expression funcExpression;
    private List<Expression> arguments;

    public FunctionCall(Expression funcExpression, List<Expression> arguments) {
        this.funcExpression = funcExpression;
        this.arguments = arguments;
    }

    @Override
    public Object evaluate(ExecutionContext ctx) {
        //Function f = ctx.getFunction(name);
        Function f = (Function) funcExpression.evaluate(ctx);
        List<String> argumentNames = f.getArguments();
        Statement functionBody = f.getBody();
        /* Should check that argument lists have the same length */
        Object[] argumentValues = new Object[arguments.size()];
        for (int i = 0; i < arguments.size(); i++) {
            argumentValues[i] = arguments.get(i).evaluate(ctx);
        }
        /* Join them? */
        ctx.pushFrame();
        ctx.setBinding(f.getBinding());
        for (int i = 0; i < arguments.size(); i++) {
            String argumentName = argumentNames.get(i);
            ctx.setVariable(argumentName, argumentValues[i]);
        }
        FlowAction action = functionBody.execute(ctx);
        assert (action == FlowAction.RETURN || action == FlowAction.NONE);
        return ctx.popFrame();
    }

    @Override
    public void generate(Generator g) {
        g.write("(");
        funcExpression.generate(g);
        g.write("(");
        boolean first = true;
        for(Expression expression : arguments) {
            if (!first) {
                g.write(", ");
            }
            expression.generate(g);
            first = false;
        }
        g.write("))");
    }
}

/*
Lambda capturing vs binding
    () ~> [ ... ]
        Copy (capture) variables by name
    () -> [ ... ]
        Bind context
Expr capture f(@v)
    f(a+b) == f(()~>[return a + b;])
Try/catch
    try [...] catch [...] finally [...]
*/