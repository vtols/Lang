package ast.expression;

import ast.exec.ExecutionContext;
import ast.generator.Generator;

import java.util.List;

public class Unary implements Expression{
    private Expression expression;
    private UnaryOperation operation;

    public Unary(Expression expression, UnaryOperation operation) {
        this.expression = expression;
        this.operation = operation;
    }

    @Override
    public Object evaluate(ExecutionContext ctx) {
        Object expr = expression.evaluate(ctx);
        switch (operation) {
            case NOT:
                if (expr instanceof  Integer) {
                    return !expr.equals(0);
                }
                if (expr instanceof List) {
                    return ((List) expr).isEmpty();
                }
                if (expr instanceof String) {
                    return ((String) expr).isEmpty();
                }
                return !(boolean) expr;
            case MINUS:
                return -(int) expr;
        }
        return null;
    }

    @Override
    public void generate(Generator g) {
        g.write("(");
        switch (operation) {
            case NOT:
                g.write("not ");
                break;
            case MINUS:
                g.write("-");
                break;
        }
        expression.generate(g);
        g.write(")");
    }
}
