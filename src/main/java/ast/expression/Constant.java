package ast.expression;

import ast.exec.ExecutionContext;
import ast.generator.Generator;

public class Constant implements Expression {
    private Object value;

    public Constant(Object value) {
        this.value = value;
    }

    @Override
    public Object evaluate(ExecutionContext ctx) {
        return value;
    }

    @Override
    public void generate(Generator g) {
        g.write(value.toString());
    }
}
