package ast.expression;

import ast.exec.ExecutionContext;
import ast.generator.Generator;

import java.util.ArrayList;
import java.util.List;

public class Binary implements Expression {
    private Expression left;
    private Expression right;
    private BinaryOperation operation;

    public Binary(Expression left, Expression right, BinaryOperation operation) {
        this.left = left;
        this.right = right;
        this.operation = operation;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object evaluate(ExecutionContext ctx) {
        Object leftValue = left.evaluate(ctx);
        Object rightValue = right.evaluate(ctx);
        switch (operation) {
            case ADD:
                if (leftValue instanceof  Integer) {
                    return (int) leftValue + (int) rightValue;
                } else if (leftValue instanceof String) {
                    return (String) leftValue + rightValue;
                } else if (leftValue instanceof List) {
                    List result = new ArrayList();
                    result.addAll((List) leftValue);
                    result.addAll((List) rightValue);
                    return result;
                }
                return null;
            case SUBTRACT:
                return (int) leftValue - (int) rightValue;
            case MULTIPLY:
                return (int) leftValue * (int) rightValue;
            case DIVISION:
                return (int) leftValue / (int) rightValue;
            case REMAINDER:
                return (int) leftValue % (int) rightValue;
            case EQ:
                return leftValue.equals(rightValue);
            case NE:
                return !leftValue.equals(rightValue);
            case LE:
                return ((Comparable) leftValue).compareTo(rightValue) < 0;
            case LEQ:
                return ((Comparable) leftValue).compareTo(rightValue) <= 0;
            case GE:
                return ((Comparable) leftValue).compareTo(rightValue) > 0;
            case GEQ:
                return ((Comparable) leftValue).compareTo(rightValue) >= 0;
        }
        return null;
    }

    @Override
    public void generate(Generator g) {
        g.write("(");
        left.generate(g);
        switch (operation) {
            case ADD:
                g.write(" + ");
                break;
            case SUBTRACT:
                g.write(" - ");
                break;
            case MULTIPLY:
                g.write(" * ");
                break;
            case DIVISION:
                g.write(" / ");
                break;
            case REMAINDER:
                g.write(" % ");
                break;
            case EQ:
                g.write(" == ");
                break;
            case NE:
                g.write(" != ");
                break;
            case LE:
                g.write(" < ");
                break;
            case LEQ:
                g.write(" <= ");
                break;
            case GE:
                g.write(" > ");
                break;
            case GEQ:
                g.write(" >= ");
                break;
        }
        right.generate(g);
        g.write(")");
    }
}
