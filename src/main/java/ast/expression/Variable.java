package ast.expression;

import ast.exec.ExecutionContext;
import ast.generator.Generator;

public class Variable implements Expression {
    private String name;

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public Object evaluate(ExecutionContext ctx) {
        return ctx.getVariable(name);
    }

    @Override
    public void generate(Generator g) {
        g.write(name);
    }
}
