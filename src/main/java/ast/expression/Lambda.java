package ast.expression;

import ast.data.Function;
import ast.exec.ExecutionContext;
import ast.generator.Generator;
import ast.statement.Statement;

import java.util.List;

public class Lambda implements Expression {
    private List<String> arguments;
    private Statement body;

    public Lambda(List<String> arguments, Statement body) {
        this.arguments = arguments;
        this.body = body;
    }

    /* Should return function */
    @Override
    public Object evaluate(ExecutionContext ctx) {
        Function f = new Function("", arguments, body);
        f.setBinding(ctx.createBinding());
        return f;
    }

    @Override
    public void generate(Generator g) {

    }
}
