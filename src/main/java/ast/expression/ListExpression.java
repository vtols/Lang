package ast.expression;

import ast.exec.ExecutionContext;
import ast.generator.Generator;

import java.util.ArrayList;
import java.util.List;

public class ListExpression implements Expression {
    private List<Expression> expressionList;

    public ListExpression(List<Expression> expressionList) {
        this.expressionList = expressionList;
    }

    @Override
    public Object evaluate(ExecutionContext ctx) {
        List<Object> result = new ArrayList<>();
        for(Expression expression : expressionList) {
            result.add(expression.evaluate(ctx));
        }
        return result;
    }

    @Override
    public void generate(Generator g) {
        g.write("[");
        boolean first = true;
        for(Expression expression : expressionList) {
            if (!first) {
                g.write(", ");
            }
            expression.generate(g);
            first = false;
        }
        g.write("]");
    }
}
