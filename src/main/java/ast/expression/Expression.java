package ast.expression;

import ast.exec.ExecutionContext;
import ast.generator.Writeable;

public interface Expression extends Writeable {
    Object evaluate(ExecutionContext ctx);
}
