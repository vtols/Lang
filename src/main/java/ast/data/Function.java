package ast.data;

import ast.exec.Environment;
import ast.generator.Generator;
import ast.generator.Writeable;
import ast.statement.Statement;

import java.util.List;

public class Function implements Writeable {
    private String name;
    private List<String> arguments;
    private Statement body;
    private Environment binding;

    public Function(String name, List<String> arguments, Statement body) {
        this.name = name;
        this.arguments = arguments;
        this.body = body;
    }

    public void setBinding(Environment binding) {
        this.binding = binding;
    }

    public String getName() {
        return name;
    }

    public List<String> getArguments() {
        return arguments;
    }

    public Statement getBody() {
        return body;
    }

    public Environment getBinding() {
        return binding;
    }

    @Override
    public void generate(Generator g) {
        StringBuilder args = new StringBuilder();
        for (String arg: arguments) {
            args.append((args.toString().equals("")) ? arg : (", ") + arg);
        }
        g.writeLine("def " + name + "(" + args + "):");
        g.indent();
        body.generate(g);
        g.unindent();
    }
}
