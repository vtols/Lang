package ast.statement;

import ast.exec.ExecutionContext;
import ast.generator.Writeable;

public interface Statement extends Writeable {
    FlowAction execute(ExecutionContext ctx);
}
