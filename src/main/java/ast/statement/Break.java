package ast.statement;

import ast.exec.ExecutionContext;
import ast.generator.Generator;

public class Break implements Statement {
    @Override
    public FlowAction execute(ExecutionContext ctx) {
        return FlowAction.BREAK;
    }

    @Override
    public void generate(Generator g) {
        g.writeLine("break");
    }
}
