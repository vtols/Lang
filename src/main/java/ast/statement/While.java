package ast.statement;

import ast.exec.ExecutionContext;
import ast.expression.Expression;
import ast.generator.Generator;

public class While implements Statement {
    private Expression condition;
    private Statement body;

    public While(Expression condition, Statement body) {
        this.condition = condition;
        this.body = body;
    }

    @Override
    public FlowAction execute(ExecutionContext ctx) {
        Object condValue = condition.evaluate(ctx);
        while (ctx.isTrue(condValue)) {
            FlowAction action = body.execute(ctx);
            if (action == FlowAction.BREAK) {
                break;
            } else if (action == FlowAction.CONTINUE) {
                continue;
            } else if (action == FlowAction.RETURN) {
                return action;
            }
            condValue = condition.evaluate(ctx);
        }
        return FlowAction.NONE;
    }

    @Override
    public void generate(Generator g) {
        g.writeIndent();
        g.write("while ");
        condition.generate(g);
        g.write(":");
        g.write("\n");
        g.indent();
        body.generate(g);
        g.unindent();
    }
}
