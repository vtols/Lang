package ast.statement;

import ast.data.Function;
import ast.exec.ExecutionContext;
import ast.generator.Generator;

import java.util.List;

public class Program implements Statement {
    private List<Function> functions;

    public Program(List<Function> functions) {
        this.functions = functions;
    }

    @Override
    public FlowAction execute(ExecutionContext ctx) {
        for (Function f: functions) {
            ctx.addFunction(f);
        }
        ctx.start();
        return FlowAction.NONE;
    }

    @Override
    public void generate(Generator g) {
        g.writeLine("println = print\n" +
                "def makeIterator(param):\n" +
                "    if type(param) is int:\n" +
                "        return range(param + 1)\n" +
                "    return param");
        for (Function f: functions) {
            f.generate(g);
        }
        g.writeLine("if __name__ == '__main__':");
        g.indent();
        g.writeLine("main()");
        g.unindent();
    }
}
