package ast.statement;

import ast.exec.ExecutionContext;
import ast.generator.Generator;

import java.util.List;

public class Block implements Statement {
    private List<Statement> statements;

    public Block(List<Statement> statements) {
        this.statements = statements;
    }

    public List<Statement> getStatements() {
        return statements;
    }

    @Override
    public FlowAction execute(ExecutionContext ctx) {
        for (Statement st : statements) {
            FlowAction action = st.execute(ctx);
            if (action != FlowAction.NONE) {
                return action;
            }
        }
        return FlowAction.NONE;
    }

    @Override
    public void generate(Generator g) {
        if (statements.isEmpty()) {
            g.writeLine("pass");
            return;
        }
        for (Statement st : statements) {
            st.generate(g);
        }
    }
}
