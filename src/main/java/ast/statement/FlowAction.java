package ast.statement;

public enum FlowAction {
    CONTINUE, BREAK, RETURN, NONE
}
