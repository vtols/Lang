package ast.statement;

import ast.exec.ExecutionContext;
import ast.expression.Expression;
import ast.generator.Generator;

public class Assign extends SimpleStatement {
    private String variableName;
    private Expression expression;

    public Assign(String to, Expression from) {
        this.variableName = to;
        this.expression = from;
    }

    @Override
    public void executeSimple(ExecutionContext ctx) {
        ctx.setVariable(variableName, expression.evaluate(ctx));
    }

    @Override
    public void generate(Generator g) {
        g.writeIndent();
        g.write(variableName + " = ");
        expression.generate(g);
        g.write("\n");
    }
}
