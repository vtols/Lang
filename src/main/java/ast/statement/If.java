package ast.statement;

import ast.exec.ExecutionContext;
import ast.expression.Expression;
import ast.generator.Generator;

public class If implements Statement {
    private Expression condition;
    private Statement trueBody;
    private Statement falseBody;

    public If(Expression condition, Statement trueBody, Statement falseBody) {
        this.condition = condition;
        this.trueBody = trueBody;
        this.falseBody = falseBody;
    }

    public If(Expression condition, Block body) {
        this(condition, body, null);
    }

    @Override
    public FlowAction execute(ExecutionContext ctx) {
        Object condValue = condition.evaluate(ctx);
        if (ctx.isTrue(condValue)) {
            return trueBody.execute(ctx);
        }
        if (falseBody != null) {
            return falseBody.execute(ctx);
        }
        return FlowAction.NONE;
    }

    @Override
    public void generate(Generator g) {
        g.writeIndent();
        g.write("if ");
        condition.generate(g);
        g.write(":\n");
        g.indent();
        trueBody.generate(g);
        g.unindent();
        if (falseBody != null) {
            g.writeLine("else:");
            g.indent();
            falseBody.generate(g);
            g.unindent();
        }
    }
}
