package ast.statement;

import ast.exec.ExecutionContext;
import ast.expression.Expression;
import ast.generator.Generator;

import java.util.Iterator;

public class For implements Statement {
    private String variableName;
    private Expression iterableExpresion;
    private Statement body;

    public For(String variableName, Expression iterableExpresion, Statement body) {
        this.variableName = variableName;
        this.iterableExpresion = iterableExpresion;
        this.body = body;
    }

    @Override
    public FlowAction execute(ExecutionContext ctx) {
        Object iterable = iterableExpresion.evaluate(ctx);
        Iterator<Object> it = ctx.getIterator(iterable);
        while (it.hasNext()) {
            Object current = it.next();
            ctx.setVariable(variableName, current);
            FlowAction action = body.execute(ctx);
            if (action == FlowAction.BREAK) {
                break;
            } else if (action == FlowAction.CONTINUE) {
                continue;
            } else if (action == FlowAction.RETURN) {
                return action;
            }
        }
        return FlowAction.NONE;
    }

    @Override
    public void generate(Generator g) {
        g.writeIndent();
        g.write("for " + variableName + " in " + "makeIterator(");
        iterableExpresion.generate(g);
        g.write("):\n");
        g.indent();
        body.generate(g);
        g.unindent();
    }
}
