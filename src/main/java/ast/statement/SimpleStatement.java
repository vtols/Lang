package ast.statement;

import ast.exec.ExecutionContext;

public abstract class SimpleStatement implements Statement {
    @Override
    public FlowAction execute(ExecutionContext ctx) {
        executeSimple(ctx);
        return FlowAction.NONE;
    }

    public abstract void executeSimple(ExecutionContext ctx);
}
