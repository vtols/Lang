package ast.statement;

import ast.exec.ExecutionContext;
import ast.generator.Generator;

public class Continue implements Statement {
    @Override
    public FlowAction execute(ExecutionContext ctx) {
        return FlowAction.CONTINUE;
    }

    @Override
    public void generate(Generator g) {
        g.writeLine("continue");
    }
}
