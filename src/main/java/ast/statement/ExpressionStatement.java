package ast.statement;

import ast.exec.ExecutionContext;
import ast.expression.Expression;
import ast.generator.Generator;

public class ExpressionStatement implements Statement {
    private Expression expression;

    public ExpressionStatement(Expression expression) {
        this.expression = expression;
    }

    @Override
    public FlowAction execute(ExecutionContext ctx) {
        expression.evaluate(ctx);
        return FlowAction.NONE;
    }

    @Override
    public void generate(Generator g) {
        g.writeIndent();
        expression.generate(g);
        g.write("\n");
    }
}
