package ast.statement;

import ast.exec.ExecutionContext;
import ast.expression.Expression;
import ast.generator.Generator;

public class Return implements Statement {
    private Expression returnExpression;

    public Return(Expression returnExpression) {
        this.returnExpression = returnExpression;
    }

    @Override
    public FlowAction execute(ExecutionContext ctx) {
        if (returnExpression != null) {
            ctx.setReturnValue(returnExpression.evaluate(ctx));
        }
        return FlowAction.RETURN;
    }

    @Override
    public void generate(Generator g) {
        g.writeIndent();
        g.write("return");
        if (returnExpression != null) {
            g.write(" ");
            returnExpression.generate(g);
        }
        g.write("\n");
    }
}
