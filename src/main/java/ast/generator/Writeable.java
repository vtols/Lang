package ast.generator;

public interface Writeable {
    void generate(Generator g);
}
