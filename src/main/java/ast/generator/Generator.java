package ast.generator;

public class Generator {
    private int indentSize = 0;
    private StringBuilder code = new StringBuilder();

    public void indent() {
        indentSize++;
    }

    public void unindent() {
        indentSize--;
    }

    public void writeLine(String s) {
        writeIndent();
        code.append(s).append('\n');
    }

    public void write(String s) {
        code.append(s);
    }

    public void writeIndent() {
        for (int i = 0; i < indentSize; i++) {
            code.append("    ");
        }
    }

    public String getCode() {
        return code.toString();
    }
}
