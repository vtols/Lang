// Define a grammar called Hello
grammar Lang;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines
ID: [a-zA-Z_][0-9a-zA-Z_]*;
Num: [0-9]+;
LEQ: '<=';
LE:  '<';
GEQ: '>=';
GE:  '>';
EQ:  '==';
NE:  '!=';
PLUS: '+';
MINUS: '-';
MULT: '*';
DIV: '/';
REM: '%';
STR: '"' ( '\\"' | ~('\n'|'\r') )*? '"';
NOT: '!';

program: function+;
function: 'fun' ID '(' idList ')' block;
block: statement | '[' statement* ']';
statement
 : ID '=' expr ';'                  #assignment
 | expr ';'                         #exprStatement
 | 'return' expr? ';'               #returnStatement
 | 'break' ';'                      #breakStatement
 | 'continue' ';'                   #continueStatement
 | 'if' ifTail                      #ifStatement
 | 'while' '(' condition=expr ')' block       #whileStatement
 | 'for' '(' var=ID 'in' list=expr ')' block #forStatement  //foreach
 ;

ifTail
    : '(' condition=expr ')' block (elseIf)?
    ;

elseIf
 : 'else' block     #else
 | 'elif' ifTail    #elif
 ;

exprList: ( expr (',' expr)* )?;

idList: ( ID (',' ID)* )?;

mul_op
 : MULT
 | DIV
 | REM
 ;

add_op
 : PLUS
 | MINUS
 ;

compare_op
 : LEQ
 | LE
 | GEQ
 | GE
 | EQ
 | NE
 ;

unary_op
 : NOT
 | MINUS
 ;

expr
 : unary_op expr             #unary
 | ID                 # variable
 | Num                # number
 | STR                # string
 | 'true'                       #true
 | 'false'                      #false
 | func=expr '(' exprList ')'     # functionCall
 | '(' idList ')' '->' block   # lambda
 | left=expr mul_op right=expr   # mulExpr
 | left=expr add_op right=expr   # addExpr
 | left=expr compare_op right=expr  #compareExpr
 | '{' exprList '}'     # list
 | '('expr')'         # brackets
 ;